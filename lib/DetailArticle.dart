import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

final homeIcon = 'assets/svg/homeIcon.svg';
final logoIcon = 'assets/images/logo_umma1.png';
final logoText = 'assets/images/logo_umma2.png';
final logoUmmaIcon = 'assets/svg/logoUmma.svg';
final heartHandIcon = 'assets/svg/heartHandIcon.svg';
final bookIcon = 'assets/svg/bookIcon.svg';
final academicCapIcon = 'assets/svg/academicCapIcon.svg';
final personIcon = 'assets/svg/personIcon.svg';

class DetailArticle extends StatefulWidget {
  @override
  _DetailArticleState createState() => _DetailArticleState();
}

class _DetailArticleState extends State<DetailArticle> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                logoIcon,
                height: 34,
                width: 30,
              ),
              SizedBox(
                width: 8,
              ),
              Image.asset(
                logoText,
                height: 12,
                width: 74,
              ),
            ],
          ),
          backgroundColor: Color(0xffE5E5E5),
        ),
        bottomNavigationBar: tabBarMenu(),
        body: Container(
          color: Color(0xffE5E5E5),
          child: TabBarView(
            children: [home(), heartHand(), book(), academicCap(), person()],
          ),
        ),
      ),
    );
  }

  Widget home() {
    return Container(
      color: Color(0xffE5E5E5),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    'Время намаза',
                    style: TextStyle(fontSize: 11),
                  ),
                  Text(
                    '5:24',
                    style: TextStyle(fontSize: 11),
                  ),
                  Text(
                    '7:12',
                    style: TextStyle(fontSize: 11),
                  ),
                  Text(
                    '12:43',
                    style: TextStyle(fontSize: 11),
                  ),
                  Text(
                    '15:29',
                    style: TextStyle(fontSize: 11),
                  ),
                  Text(
                    '18:11',
                    style: TextStyle(fontSize: 11),
                  ),
                  Text(
                    '19:53',
                    style: TextStyle(fontSize: 11),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: Column(
                  children: [
                    Text(
                      'Обязательны ли 5 намазов?',
                      style: TextStyle(
                        fontSize: 20,
                        color: Color(0xff3D3D3D),
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Image.asset('assets/images/img.png'),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      'Автор: Шамиль Аляутдинов',
                      style:
                          TextStyle(fontSize: 11, color: Color(0xff733C3C43)),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      'В последнее время в Рунете появляются статьи о необязательности пятикратного намаза. Хотелось бы видеть статью со ссылками на Коран и Сунну от вас по этому вопросу. Ильхом.',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                        'Глупые статьи (с богословской точки зрения). Не намерен на них реагировать. Мусульмане, к сожалению, своими же руками (обсуждениями) растаскивают это по всему Интернету. Для своей аудитории напомню в данном контексте три цитаты из Корана и Сунны, подтверждающие факт обязательности пяти намазов:')
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget heartHand() {
    return Text('heartHand');
  }

  Widget book() {
    return Text('book');
  }

  Widget academicCap() {
    return Text('academicCap');
  }

  Widget person() {
    return Text('person');
  }

  Widget tabBarMenu() {
    return Container(
      color: Color(0xffE5E5E5),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 36.0),
        child: TabBar(indicatorColor: Color(0xffE5E5E5), tabs: [
          Tab(
            icon: SvgPicture.asset(homeIcon),
          ),
          Tab(
            icon: SvgPicture.asset(heartHandIcon),
          ),
          Tab(
            icon: SvgPicture.asset(bookIcon),
          ),
          Tab(
            icon: SvgPicture.asset(academicCapIcon),
          ),
          Tab(
            icon: SvgPicture.asset(personIcon),
          ),
        ]),
      ),
    );
  }
}
