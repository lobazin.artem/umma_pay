import 'package:flutter/material.dart';
import 'package:umma_pay/DetailArticle.dart';
import 'package:umma_pay/ListArticles.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (BuildContext context) => ListArticles(),
        // '/main': (BuildContext context) => ListArticles(),
        '/detailarticle': (BuildContext context) => DetailArticle(),
      },
      // home: ListArticles(),
    );
  }
}
